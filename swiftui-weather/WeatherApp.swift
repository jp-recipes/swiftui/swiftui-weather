//
//  swiftui_weather_ios_15App.swift
//  swiftui-weather-ios-15
//
//  Created by JP on 21-08-23.
//

import SwiftUI

@main
struct WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
