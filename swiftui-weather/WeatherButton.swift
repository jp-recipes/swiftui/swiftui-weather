//
//  WeatherButton.swift
//  swiftui-weather-ios-15
//
//  Created by JP on 21-08-23.
//

import SwiftUI

struct WeatherButton: View {
    var title: String
    var textColor: Color
    var backgroundColor: Color

    var body: some View {
        Text(title)
            .frame(width: 280, height: 50)
            .background(backgroundColor)
            .foregroundColor(textColor)
            .font(.system(size: 20, weight: .bold))
            .cornerRadius(10)
    }
}


struct WeatherButton_Previews: PreviewProvider {
    static var previews: some View {
        WeatherButton(title: "Change Day Time", textColor: .blue, backgroundColor: .white)
    }
}
